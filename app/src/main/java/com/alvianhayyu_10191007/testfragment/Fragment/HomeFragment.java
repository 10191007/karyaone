package com.alvianhayyu_10191007.testfragment.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alvianhayyu_10191007.testfragment.Activity.CutiActivity;
import com.alvianhayyu_10191007.testfragment.Activity.HistoryActivity;
import com.alvianhayyu_10191007.testfragment.Activity.LoginActivity;
import com.alvianhayyu_10191007.testfragment.Activity.MainActivity;
import com.alvianhayyu_10191007.testfragment.Activity.PresensiActivity;
import com.alvianhayyu_10191007.testfragment.R;

public class HomeFragment extends Fragment {

    LinearLayout LL_presensi,LL_riwayat, LL_cuti;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        LL_presensi = view.findViewById(R.id.LL_presensi);
        LL_cuti = view.findViewById(R.id.LL_cuti);
        LL_riwayat = view.findViewById(R.id.LL_riwayat);

        LL_cuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CutiActivity.class);
                startActivity(intent);
            }
        });

        LL_presensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PresensiActivity.class);
                startActivity(intent);
            }
        });

        LL_riwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), HistoryActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }
}