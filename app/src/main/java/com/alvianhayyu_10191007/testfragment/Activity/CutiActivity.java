package com.alvianhayyu_10191007.testfragment.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.alvianhayyu_10191007.testfragment.R;

public class CutiActivity extends AppCompatActivity {

    ImageView backCuti;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuti);

        backCuti = findViewById(R.id.backCuti);
        backCuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }
}